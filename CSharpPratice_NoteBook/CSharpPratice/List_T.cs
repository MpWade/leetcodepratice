﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimothyLiu_CSharpPratice
{
    class List_T
    {
        //static void Main(string[] args)
        //{
        //    //List Create 
        //    //int[] arry = new int[] { 1, 2, 3, 4 };
        //    //Console.WriteLine(arry is IEnumerable<int>);           
        //    //List<int> list3 = new List<int>(10); //指定List數量，避免動態擴充造成效能損失

        //    //List Add
        //    //List<int> list = new List<int>() { 1,2,3,4};            
        //    //for (int i = 1; i <= 100; i++)
        //    //{
        //    //    list.Add(i); //Add從集合尾部增加一個元素
        //    //}

        //    //List AddRange
        //    //List<int> list = new List<int>() { 1, 2, 3, 4 };
        //    //List<int> list2 = new List<int> { 10,20,30,40};
        //    //list2.AddRange(list);
        //    //Console.WriteLine($"{list2.Count } / {list2.Capacity}");
        //    //Console.WriteLine(String.Join(",",list2));

        //    //List Insert
        //    //List<int> list = new List<int>() { 1, 2, 3, 4 };
        //    //List<int> list2 = new List<int> { 10, 20, 30, 40 };
        //    //list2.AddRange(list);
        //    //list2.Insert(0, 300); //插到集合最前面，後面所有元素都必須往後移動
        //    //list2.Insert(list2.Count, 3033); //插到集合尾部，效果等於Add
        //    //Console.WriteLine($"{list2.Count } / {list2.Capacity}");
        //    //Console.WriteLine(String.Join(",", list2));

        //    //List InsertRange
        //    //List<int> list = new List<int>() { 1, 2, 3, 4 };
        //    //List<int> list2 = new List<int> { 10, 20, 30, 40 };
        //    //list2.InsertRange(2, list); //將集合指定位置插入
        //    //list2.Clear();
        //    //Console.WriteLine($"{list2.Count } / {list2.Capacity}");
        //    //Console.WriteLine(String.Join(",", list2));

        //    //List RemoveAt , RemoveRangeS
        //    //List<int> list = new List<int>() { 1, 2, 3, 4 };
        //    //List<int> list2 = new List<int> { 10, 20, 30, 40 };
        //    ////list2.RemoveAt(0); //指定位置元素刪除，並移動元素做排列
        //    //list2.RemoveRange(2, 2); //從指定Index開始刪除Count數量的元素
        //    //Console.WriteLine($"{list2.Count } / {list2.Capacity}");
        //    //Console.WriteLine(String.Join(",", list2));

        //    //List Remove & RemoveAll
        //    //List<int> list = new List<int>() { 1, 2, 3, 40 };
        //    //List<int> list2 = new List<int> { 10, 20, 30, 40 };
        //    //list2.AddRange(list);
        //    //list2.Remove(40); //刪除集合內第一個出現的元素
        //    //list2.RemoveAll(e => e == 40); //RemoveAll裡面要放判斷條件
        //    //list2.RemoveAll(e => e % 10 == 0); //RemoveAll裡面要放判斷條件
        //    //Console.WriteLine($"{list2.Count } / {list2.Capacity}");
        //    //Console.WriteLine(String.Join(",", list2));

        //    //List<int> intlist = new List<int> { 100, 200, 300, 400 }; // Value Type
        //    //List<Book> booklist = new List<Book>(); // Reference Type
        //    //for (int i = 1; i < 10; i++)
        //    //{
        //    //    booklist.Add(new Book { ID = i, Name = $"Book_{i}", Price = i * 100 });
        //    //}
        //    //Console.WriteLine($"intList:{intlist.Count}/{intlist.Capacity}");
        //    //Console.WriteLine($"bookList:{booklist.Count}/{booklist.Capacity}");
        //    //Console.WriteLine("======================");

        //    //List Readelement
        //    //Console.WriteLine(intlist[0]);
        //    //Console.WriteLine(intlist[intlist.Count-1]);
        //    //Console.WriteLine(booklist[0]);
        //    //Console.WriteLine(booklist[booklist.Count - 1]);

        //    //List GetRange
        //    //List<int> seg =  intlist.GetRange(1, 2);
        //    //for (int i = 0; i < seg.Count; i++)
        //    //{
        //    //    seg[i]++;
        //    //}
        //    ////Console.WriteLine(String.Join(",",seg)); //seg值改變，Copy到新的記憶體位置
        //    ////Console.WriteLine(String.Join(",",intlist)); //seg改變不影響intlist

        //    //var seg2 = booklist.GetRange(0, 2);
        //    //for (int i = 0; i < seg2.Count; i++)
        //    //{
        //    //    seg2[i].Price++;
        //    //}
        //    //Console.WriteLine(String.Join(",", seg2[0].Price)); 
        //    //Console.WriteLine(String.Join(",", booklist[0].Price)); //seg2值改變會影響booklist，因為都是指向同一個Heap位置的變量

        //    //List GetEnumerator
        //    //var e = intlist.GetEnumerator();
        //    //Console.WriteLine(e.Current);
        //    //while (e.MoveNext()) //MoveNext 如果集合內有值True 沒有為False
        //    //{
        //    //    Console.WriteLine(e.Current);
        //    //}
        //    //Console.WriteLine(e.Current);

        //    //int sum =0;
        //    //intlist.ForEach(value => sum += value);
        //    //Console.WriteLine(sum);

        //    List<double> list = new List<double>() { 80.0,70.0,60.0,50.0,40.0,30.0,20.0,10.0};
        //    Book book1 = new Book() { ID = 1, Name = "Book_1", Price = 10 };
        //    Book book2 = new Book() { ID = 2, Name = "Book_2", Price = 20 };
        //    Book book3 = new Book() { ID = 3, Name = "Book_3", Price = 30 };
        //    Book book4 = new Book() { ID = 4, Name = "Book_4", Price = 40 };
        //    Book book5 = new Book() { ID = 1, Name = "Book_1", Price = 10 };
        //    List<Book> books = new List<Book>() { book1, book2, book3, book4 };

        //    //list.Sort();           
        //    //int res  =list.BinarySearch(30); //BinarySearch 必須先排序才可以使用

        //    books.Sort(); //Book Class 沒有IComparable這個東西，比較去實現才可進行比較
        //    var res = books.BinarySearch(book5);

        //    Console.WriteLine(res);            
        //    Console.ReadLine();
        //}

        public class Book : IComparable<Book> // IComparable<Book>意思 只能跟Book進行比較
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public double Price { get; set; }

            public int CompareTo(Book other)
            {
                if (other == null) return 1;
                return this.ID - other.ID;               
            }

            //public override bool Equals(object obj) // 重寫Equals來比對屬性
            //{
            //    if(obj == null) return false;
            //    Book book = obj as Book;
            //    if(book == null) return false;
            //    if(book.ID == this.ID && book.Name == this.Name && book.Price == this.Price) return true;
            //    return false;
            //}

            //public override string ToString()
            //{

            //}
        }

    }
}
