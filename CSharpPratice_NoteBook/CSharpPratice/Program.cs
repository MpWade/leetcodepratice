﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimothyLiu_CSharpPratice
{
    class Program
    {
        //static void Main(string[] args)
        //{            
        //    //LINQ:.NET Language Intergrated Query            
        //    //Console.WriteLine(smoothies.ingredients);
        //}
        /// <summary>
        /// 考拉茲猜想:如果正整數N是偶數就 N/2 奇數 3N+1 一直到N = 1
        /// 回傳 N = 1 需要幾個步驟
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static int collatz(int num)
        {
            //遞歸用法
            //int k = num % 2 < 1 ? num / 2 : 3 * num + 1;
            //return num < 2 ? 0 : collatz(k) + 1;

            int count = 0;
            while (num != 1)
            {
                if (num % 2 == 0)
                {
                    num = num / 2;
                    count++;
                    continue;
                }
                if (num % 2 == 1)
                {
                    num = num * 3 + 1;
                    count++;
                    continue;
                }
            }
            return count;
        }
        /// <summary>
        /// 輸入整數判斷是不是3,5,15的倍數
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string FizzBuzz(int n)
        {
            if (n % 15 == 0)
            {
                return "FizzBuzz";
            }
            else if (n % 5 == 0)
            {
                return "Buzz";
            }
            else if (n % 3 == 0)
            {
                return "Fizz";
            }
            return n.ToString();
        }
        public static string Stutter(string word)
        {

            string w = word[0].ToString() + word[1] + "... " + word[0] + word[1] + "... " + word + "?";
            return w;
        }
        public static int Equal(int a, int b, int c)
        {
            //?運算子
            //return a == b && b == c ? 3 : a == b || b == c ? 2 : 0;
            //LINQ解法
            int[] arr = new int[] { a, b, c };
            int count = arr.GroupBy(i => i).Where(i => i.Count() > 1).Sum(i => i.Count());

            return count;
        }
        /// <summary>
        /// 階層運算，遞歸練習
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static int factorial(int num)
        {
            //以3階層為例子 先呼喊再聽到回音
            //num = 3 呼叫自己使得num - 1 = 2 得到 3 * 2 = 6
            //num = 2 ,  num - 1 = 1 return  2 * 1 
            //num = 1 return 1 給 num = 2 使用
            if (num == 1)
            {
                return 1;
            }
            else
            {

                return num * factorial(num - 1);
            }
        }
        /// <summary>
        /// 找出1~10之間沒有出現的數字(只會有一個數字沒出現)
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static int MissingNum(int[] arr)
        {
            return 55 - arr.Sum();
        }
        #region LINQ_OrderBy練習
        void OrderBy()
        {
            int[] numbers = { 5, 10, 8, 3, 6, 12 };

            //Query syntax:
            IEnumerable<int> numQuery1 =
                from num in numbers
                where num % 2 == 0
                orderby num
                select num;

            //Method syntax:
            IEnumerable<int> numQuery2 = numbers.Where(num => num % 2 == 0).OrderBy(num => num);

            foreach (int i in numQuery1)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine(System.Environment.NewLine);
            foreach (int i in numQuery2)
            {
                Console.Write(i + " ");
            }
            // Keep the console open in debug mode.
            Console.WriteLine(System.Environment.NewLine);
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
        #endregion
        #region 泛型屬性練習
        class Apple
        {
            public string Color { get; set; }
        }
        class Book
        {
            public string Name { get; set; }
        }
        class Box<TCargo>
        {
            public TCargo Cargo { get; set; }
        }
        class AppleBox
        {
            public Apple Cargo { get; set; }
        }
        class BookBox
        {
            public Book Cargo { get; set; }
        }
        #endregion
        #region 泛型interface練習
        interface IUnique<TID>
        {
            TID ID { get; set; } //interface 存取修飾詞默認是public 
        }

        class Student<TID> : IUnique<TID> //繼承泛型interface若無指定類型 <TID>
                                          //類型Student後面要加上<TID>
        {
            public TID ID { get; set; }
            public string Name { get; set; }
        }
        class Person : IUnique<int> //繼承泛型interface指定類型 
        {
            public int ID { get; set; }
        }
        #endregion
        #region 泛型內建集合using System.Collections.Generic;練習
        class PraticeGeneric
        {
            void Partice()
            {
                IList<int> list = new List<int>();
                for (int i = 0; i < 100; i++)
                {
                    list.Add(i);
                }
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }

                IDictionary<int, string> dict = new Dictionary<int, string>();
                dict[1] = "Wade";
                dict[2] = "Cindy";

                Console.WriteLine($"Studen1 = {dict[1]}");
            }

        }
        #endregion
        #region 泛型委託練習

        static void Say(string str)
        {
            Console.WriteLine($"Hello,{str}!");
        }
        static void Mul(int x)
        {
            Console.WriteLine(x * 100);
        }
        static int Add(int a, int b)
        {
            return a + b;
        }
        static double Add(double a, double b)
        {
            return a + b;
        }
        #endregion
        #region Enum練習
        enum Level
        {
            Employee, Manager, Boss, BigBoss,
        }

        enum Skill
        {
            Drive = 1, Cook = 2, Program = 4, Teach = 8,
        }
        class Employee
        {
            public Skill Skill { get; set; }
            public Level Level { get; set; }
        }
        #endregion

    }

    class Circle
    {
        private double Radius { get;}
        public Circle(double radius)
        {
            Radius = radius;
        }
        public double GetArea() => Radius * Radius * Math.PI;
       
        public double GetPerimeter() => Radius* 2*Math.PI;
       
    }
   



}
