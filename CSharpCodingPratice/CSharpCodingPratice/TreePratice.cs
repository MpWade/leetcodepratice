﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCodingPratice
{
    public class TreePratice
    {
        //static void Main(string[] args)
        //{
        //    ////先new出一個Employee物件叫a
        //    //Employee a = new Employee("a", 95, 25);

        //    ////以我們的二元樹類別，傳入的型別是Employee，預設建構子放a物件。
        //    //Tree<Employee> tree1 = new Tree<Employee>(a);
            
        //    //Employee b = new Employee("b", 120, 10);
        //    //tree1.Insert(b);
        //    //Employee e = new Employee("e", 125, 100);
        //    //tree1.Insert(e);
        //    //Employee c = new Employee("c", 500, 7);
        //    //tree1.Insert(c);
        //    //Employee d = new Employee("d", 95, 10);
        //    //tree1.Insert(d);
        //    //Employee f = new Employee("f", 555, 10);
        //    //tree1.Insert(f);

        //    Tree<float> tree = new Tree<float>(6f);
        //    tree.Insert(3333.3f);
        //    tree.Insert(6.3f);
        //    tree.Insert(5.3f);
        //    tree.Insert(9.3f);
        //    tree.Insert(13.3f);
        //    //將我們要的結果印出來
        //    tree.WalkTree();
        //}

        public class Tree<TItem> where TItem : IComparable<TItem>
        {
            //where TItem : IComparable<TItem>的意思是 
            //傳進來的TItem型別必須實作IComparable<>介面
            //每個節點都要有三個屬性，1.NodeDate 2.左邊節點資料 3.右邊節點資料
            public TItem NodeData { get; set; }
            public Tree<TItem> LeftTree { get; set; } //樹的左邊
            public Tree<TItem> RightTree { get; set; } //樹的右邊


            public Tree(TItem nodeValue)
            {
                this.NodeData = nodeValue;
                this.LeftTree = null;
                this.RightTree = null;
            }

            //做一個Insert()的方法，傳入的參數為<TItem>型別的物件
            public void Insert(TItem newItem)
            {
                TItem currentNodeValue = this.NodeData;

                //跟節點比大小，如果比節點小就往左走，比節點大就往右走。
                //注意這邊的CompareTo()方法，就是等等傳入類別必須實作IComparable<TItem>介面
                //的CompareTo()方法
                if (currentNodeValue.CompareTo(newItem) > 0)
                {
                    //如果左節點是空的，就占下來，反之，則在跟下一層的節點比，也就是遞迴直到找到位置為止
                    if (this.LeftTree == null)
                    {
                        this.LeftTree = new Tree<TItem>(newItem);
                    }
                    else
                    {
                        this.LeftTree.Insert(newItem);
                    }
                }
                else
                {
                    //如果右節點是空的，就占下來，反之，則在跟下一層的節點比，也就是遞迴直到找到位置為止
                    if (this.RightTree == null)
                    {
                        this.RightTree = new Tree<TItem>(newItem);
                    }
                    else
                    {
                        this.RightTree.Insert(newItem);
                    }
                }
            }


            //做一個WalkTree()方法，依左中右的順序從最下層開始找二元樹的節點。
            //這個方法是讓我們可以印出二元樹各節點的值。
            public void WalkTree()
            {
                if (this.LeftTree != null)
                {
                    this.LeftTree.WalkTree();
                }

                Console.WriteLine(this.NodeData.ToString());

                if (this.RightTree != null)
                {
                    this.RightTree.WalkTree();
                }
            }
        }

        public class Employee : IComparable<Employee>
        {
            private string name { get; set; }
            private int salary { get; set; }
            private int hour { get; set; }

            public Employee(string name, int salary, int hour)
            {
                this.name = name;
                this.salary = salary;
                this.hour = hour;
            }

            //override  ToString()方法，傳回某某某的薪水為多少元
            public override string ToString()
            {
                return this.name + " 的薪水為: " + this.hour * this.salary + "元";
            }


            //實作IComparable<Employee>介面的方法
            //自己定義要怎麼比較，時薪*工時的結果
            public int CompareTo(Employee other)
            {
                if (this.hour * this.salary == other.salary * other.hour) { return 0; }
                if (this.hour * this.salary > other.salary * other.hour) { return 1; }
                return -1;
            }           
        }
    }
}
