﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCodingPratice
{
    internal class ArrayPratice
    {
        static void Main(string[] args)
        {
          
        }
        /// <summary>
        /// 1920. Build Array from Permutation
        /// </summary>
        /// <param name="nums"></param>
        /// <returns></returns>       
        public int[] BuildArray(int[] nums)
        {
            // 取出每個陣列的值
            // 將取出的值從頭開始放入陣列內
            //int[] result = new int[nums.Length];          
            for (int i = 0; i < nums.Length; i++)
            {
                nums[i] = nums[nums[i]];
            }
            return nums;
        }
        /// <summary>
        /// 1929. Concatenation of Array
        /// </summary>
        /// <param name="nums"></param>
        /// <returns></returns>
        public int[] GetConcatenation(int[] nums)
        {
            int n =nums.Length;
            int[] result = new int[n * 2];

            //將nums每個值再複製一次給nums
            for (int i = 0; i < n; i++)
            {
                result[i] = nums[i];
                result[i + n] = nums[i];
            }
            return result;
            
        }

        public void Test()
        {

        }       
        
    }
}
