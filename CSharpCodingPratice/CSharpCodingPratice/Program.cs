﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;


namespace CSharpCodingPratice
{
    class Program
    {
        //static void Main(string[] args)
        //{         
        //    ////Task.Start() => 不等
        //    //Task slowTask = new Task(() => RunSlowly("【7】我是 SlowTask 我等 1 秒"));
        //    //slowTask.Start();

        //    ////Task.Factory.StartNew() => 不等
        //    //Task.Factory.StartNew(() => RunQuickly("【3】我是 QuickTask 我等 0.3 秒"));
        //    //Console.WriteLine("【1】我是主程式 我不用等 slowTask 跟 quickTask 我會最先跑");



        //    ////【Run → Result】-------------------------------------------------------------------------------
        //    ////Task.Run() => 不等
        //    //var mediumTask = Task.Run<string>(() => { return GetReturn("【4】我是 MediumTask 我等 0.6 秒"); });
        //    //Console.WriteLine("【2】並不是 Task.Run 阻塞主程式");

        //    ////Task.Result => 要等
        //    //Console.WriteLine(mediumTask.Result);
        //    //Console.WriteLine("【5】而是 MediumTask.Result 讓主程式被迫等待");
        //    ////------------------------------------------------------------------------------------------------



        //    ////【Start → Wait】-------------------------------------------------------------------------------
        //    ////Task.Start() => 不等
        //    //Task dbTask = new Task(() => GetDataFromDb("【8】花了 2 秒鐘將資料從 DB 取回"));
        //    //dbTask.Start();
        //    //Console.WriteLine("【6】並不是 Task.Start 阻塞主程式");
        //    ////Task.Wait() => 要等
        //    //dbTask.Wait();
        //    //Console.WriteLine("【9】而是 Task.Wait() 讓主程式被迫等待");
        //    ////------------------------------------------------------------------------------------------------


        //    //Console.Read();

        //    //Program program = new Program();

        //    //int[] i = new int[] { 11, 5, 7, 11, 31, 6, 8 };
        //    //Console.WriteLine(program.MaxProfit(i));
        //}
        static void RunQuickly(string str)
        {
            Thread.Sleep(300);
            Console.WriteLine(str);
        }

        static void RunSlowly(string str)
        {
            Thread.Sleep(1000);
            Console.WriteLine(str);
        }

        static string GetReturn(string str)
        {
            Thread.Sleep(600);
            return str;
        }

        static void GetDataFromDb(string str)
        {
            Thread.Sleep(2000);
            Console.WriteLine(str);
        }





        public int MaxSubArray(int[] nums)
        {           

            int sum = 0;
            int max = nums[0];
            for (int i = 0; i < nums.Length; ++i)
            {
                sum += nums[i];
                sum = Math.Max(0, sum); //大於0取值
                max = Math.Max(sum, max);
            }
            return max;

            int currentsum = 0;
            int max2 = nums[0];
            for (int k = 0; k < nums.Length; k++)
            {
                if (nums[k]+ currentsum > nums[k]) //目前總和+新數字 > 新數字 ， 總和繼續相加
                {
                    currentsum += nums[k]; 
                }
                else //把新數字當作起點
                {
                    currentsum = nums[k];
                }
                if (currentsum > max2)
                {
                    max2 = currentsum;
                }
                
            }
            return max2;
        }
        public int MaxProfit(int[] prices)
        {
            //抓到最小值
            //去比較之後最大值
            int min = prices[0];
            int max = 0;
            for (int i = 1; i < prices.Length; i++)
            {
                min = Math.Min(min, prices[i]);
                max = Math.Max(max, prices[i] - min);
            }
            return max;
        }
        public int SearchInsert(int[] nums, int target)
        {
            int a = nums.Length; ;
            for (int i = 0; i < nums.Length; i++)
            {

                if (nums[i] == target)
                {
                    return i;
                }
                else if (target < nums[i])
                {
                    a = i;
                    return a;
                }
            }
            return a;
        }
        public static int PivotIndex(int[] nums)
        {
            int total = nums.Sum();
            int sum = 0;
            for (int i = 0; i < nums.Length; ++i)
            {
                if (2 * sum + nums[i] == total)
                {
                    return i;
                }
                sum += nums[i];
            }
            return -1;
        }

        public static int[] ReplaceElements(int[] arr)
        {
            var max = -1;
            for (var i = arr.Length - 1; i >= 0; i--) //從最後面比較回來
            {
                var tmp = arr[i]; //紀錄最後一項數字
                arr[i] = max;  //最後一項賦值
                if (tmp > max) //比較大小
                {
                    max = tmp;
                }
            }
            return arr;
        }
        public static string LongestCommonPrefix(string[] strs)
        {
            if (strs.Length == 0) return "";
            string prefix = strs[0];
            for (int i = 1; i < strs.Length; i++)
                while (strs[i].IndexOf(prefix) != 0)
                {
                    prefix = prefix.Substring(0, prefix.Length - 1);
                    if (string.IsNullOrEmpty(prefix)) return "";
                }
            return prefix;
        }

        public static bool IsPalindrome(int x)
        {
            char[] s;
            s = x.ToString().ToCharArray();
            int start = 0;
            int end = s.Length - 1;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[start] == s[end])
                {
                    start++;
                    end--;
                    if (start == end) return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }


        public int RomanToInt(string s)
        {
            //羅馬數字規則一定是左大於右排列相加，如果右大於左則要相減            
            int sum = 0;
            Dictionary<char, int> values = new Dictionary<char, int>();
            values.Add('M', 1000);
            values.Add('D', 500);
            values.Add('C', 100);
            values.Add('L', 50);
            values.Add('X', 10);
            values.Add('V', 5);
            values.Add('I', 1);
            for (int i = 0; i < s.Length; i++)
            {
                if (i == s.Length - 1) //判斷是否為最後元素，否則下一行判斷會錯誤
                {
                    sum += values[s[i]];
                    break;
                }
                if (values[s[i]] < values[s[i + 1]]) //判斷左邊小於右邊
                {
                    sum -= values[s[i]];
                }
                else
                {
                    sum += values[s[i]];
                }
            }
            return sum;
        }

        /// <summary>
        /// 找出兩個數相加 = Target
        /// </summary>
        /// <param name="nums"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static int[] TwoSum(int[] nums, int target)
        {
            for (int i = 0; i < nums.Length; i++)
            {
                int value = target - nums[i];
                for (int k = i + 1; k < nums.Length; k++)
                {
                    if (value - nums[k] == 0)
                    {
                        return new int[] { i, k };
                    }
                }
            }
            return null;


        }
        public static bool ValidMountainArray(int[] arr)
        {
            int N = arr.Length;
            int i = 0;
            if (arr.Length < 3) return false;
            for (int k = 0; k < N - 1; k++)
            {
                if (arr[k] == arr[k + 1]) return false;
                if (arr[k] > arr[k + 1])
                {
                    break;
                }
                i++;
            }
            if (i == 0 || i == N - 1) return false;
            for (int j = i; j < N - 1; j++)
            {
                if (arr[j] == arr[j + 1]) return false;
                if (arr[j] < arr[j + 1])
                {
                    break;
                }
                i++;
            }
            return i == N - 1;
            //// 先往上爬找到最高點
            //while (i + 1 < N && arr[i] < arr[i + 1])
            //    i++;

            //// 最高點不能是第一個或最後一個
            //if (i == 0 || i == N - 1)
            //    return false;

            //// 最高點往下走
            //while (i + 1 < N && arr[i] > arr[i + 1])
            //    i++;

            //return i == N-1; //若全部符合 i == 陣列的項目總數
        }
        public static bool CheckIfExist(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (i == j) continue;
                    if (arr[i] == arr[j] * 2)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public static int RemoveElement(int[] nums)
        {
            if (nums.Length == 0) return 0;
            int i = 0;
            for (int j = 1; j < nums.Length; j++)
            {
                if (nums[j] != nums[i])
                {
                    i++;
                    nums[i] = nums[j];
                }
            }
            return i + 1;

        }
        public static void Merge(int[] nums1, int m, int[] nums2, int n)
        {
            for (int i = 0; i < n; i++)
            {
                nums1[i + m] = nums2[i];
            }
            Array.Sort(nums1);

            //-----------------------------------------------------------------------

            //int[] newarr1 = new int[m+n];
            //int a = 0;
            //for (int i = 0; i < nums1.Length - n; i++)
            //{
            //    if (nums1[i] == 0)
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        newarr1[a] = nums1[i];
            //        a++;
            //    }
            //}

            //int[] newarr2 = new int[nums2.Length];
            //int b = 0;
            //for (int i = 0; i < nums2.Length; i++)
            //{
            //    if (nums2[i] == 0)
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        newarr2[b] = nums2[i];
            //        b++;
            //    }
            //}

            //for (int i = 0; i < n; i++)
            //{

            //    newarr1[i+m] = newarr2[i];
            //}              
            //Array.Sort(newarr1);
            //for (int i = 0; i < newarr1.Length; i++)
            //{
            //    nums1[i] = newarr1[i];
            //}
        }
        public static void BubbleSort(int[] array)
        {
            //泡泡排序:需要掃描次數，跟交換次數
            int tmp;
            for (int i = array.Length - 1; i > 0; i--) //Scan times
            {
                for (int j = 0; j < i; j++) //change times
                {
                    if (array[j] > array[j + 1])
                    {
                        tmp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = tmp;
                    }
                }
            }
        }
        public static void SelectionSort(int[] array)
        {
            int smallest;
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] > array[j])
                    {
                        smallest = array[i];
                        array[i] = array[j];
                        array[j] = smallest;
                    }
                }
            }
        }

        public static void DuplicateZeros(int[] arr)
        {
            int pointer = 0;
            int a = 0;
            int[] newarr = new int[arr.Length * 2];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 0)
                {
                    newarr[a] = 0;
                    a++;
                    newarr[a] = 0;
                    pointer += 2;
                }
                else
                {
                    newarr[a] = arr[i];
                    pointer++;
                }
                a++;
            }
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = newarr[i];
            }

        }
        public static int[] SortedSquares(int[] nums)
        {
            int[] SortedSquaresArr = new int[nums.Length];
            for (int i = 0; i < nums.Length; i++)
            {
                SortedSquaresArr[i] = nums[i] * nums[i];
            }
            Array.Sort(SortedSquaresArr);
            return SortedSquaresArr;
        }
        public static int FindEvenNumbers(int[] nums)
        {
            int result = 0;
            int count = 0;
            int ii;
            for (int i = 0; i < nums.Length; i++)
            {
                while (true)
                {
                    count++;
                    ii = nums[i] / 10;
                    nums[i] = ii;
                    if (nums[i] == 0)
                    {
                        if (count % 2 == 0)
                        {
                            result++;
                        }
                        count = 0;
                        break;
                    }

                }
            }
            return result;
        }
        public static int Fionacci(int n)
        {
            if (n == 0)
            {
                return 0;
            }
            else if (n == 1)
            {
                return 1;
            }
            else
            {
                return (Fionacci(n - 1) + Fionacci(n - 2));
            }
        }
        public static int CountClaps(string txt)
        {
            string input = txt;
            string pattern = @"([C])";
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection mc = regex.Matches(input);
            return mc.Count;
        }

        /// <summary>
        /// 圓外接正方形，與內接正方形面積差
        /// </summary>
        /// <param name="radius"></param>
        /// <returns></returns>
        public static int square_areas_difference(int radius)
        {
            return 2 * radius * radius;
        }
        /// <summary>
        /// 找出每個不規則陣列的最大值，並轉成陣列呈現
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static double[] FindLargest(double[][] values)
        {
            return values.Select(v => v.Max()).ToArray();

            //double[] maxNumbers = new double[values.Length];

            //int i = 0;
            //foreach (double[] array in values)
            //{
            //    maxNumbers[i] = array.Max();
            //    i++;
            //}

            //return maxNumbers;
        }

        /// <summary>
        /// 可將所有陣列轉變成字串陣列
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static string[] ParseArray(object[] arr)
        {
            return Array.ConvertAll(arr, x => x.ToString());
            //return arr.Select(o => o.ToString()).ToArray();
        }
        public static int[] SortNumsAscending(int[] arr)
        {
            if (arr == null || arr.Length == 0)
            {
                return arr;
            }
            Array.Sort(arr);
            return arr;
        }
        /// <summary>
        /// 比對字串內文字是否全部相同
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool isIdentical(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != str[0])
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 比對字串內是否有aeiou
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int CountVowels(string str)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u')
                {
                    count++;
                }
            }
            return count;
        }
        public static int CountVowelsRegex(string str)
        {
            string input = str;
            string pattern = @"([aeiou]|[AEIOU])";
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection mc = regex.Matches(input);
            return mc.Count;
        }
        public static int Factorial(int num)
        {
            if (num == 0) return 1;
            return Factorial(num - 1) * num;
        }
        public static int CountDs(string str) //Calculate d's count
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'd' || str[i] == 'D')
                {
                    count++;
                }
            }
            return count;
        }
        public static string Bomb(string txt) //Contains String 
        {
            return txt.ToLower().Contains("bomb") ? "Duck!!!" : "There is no bomb, relax.";
        }
        public static int Search(int[] arr, int item)
        {
            if (arr.Length == 0) return -1;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == item)
                {
                    return Array.IndexOf(arr, item);
                }
            }
            return -1;
        }
        public static int RemoveDuplicates(int[] nums) //RemoveDuplicates index in Array
        {
            if (nums.Length == 0) return 0;
            int i = 0;
            for (int j = 1; j < nums.Length; j++)
            {
                if (nums[j] != nums[i])
                {
                    i++;
                    nums[i] = nums[j];
                }
            }
            return i + 1;
        }
        public static int FindMaxConsecutiveOnes(int[] nums)
        {
            int count = 0;
            int result = 0;
            if (nums.Length == 0) return 0;
            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] != 1)
                {
                    count = 0;
                    continue;
                }
                else
                {
                    count++;
                    if (count > result)
                    {
                        result = count;
                    }
                }
            }
            return result;
        }
    }
    public class Customer //事件擁有者
    {
        #region 事件完整聲明
        //private OrderEventHandler orderEventHandler; 
        //public event OrderEventHandler Order 
        //{
        //    add
        //    {
        //        this.orderEventHandler += value; 
        //    }
        //    remove
        //    {
        //        this.orderEventHandler -= value;
        //    }
        //}
        #endregion
        #region 事件簡易聲明
        public event OrderEventHandler Order;

        #endregion
        public double Bill { get; set; }
        public void PayTheBill()
        {
            Console.WriteLine("I will pay ${0}", this.Bill);
        }

        public void WalkIn()
        {
            Console.WriteLine("Walk into the restaurant");
        }

        public void SitDown()
        {
            Console.WriteLine("Sit Down.");
        }

        public void Think()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Let me think...");
                Thread.Sleep(1000);
            }
            this.OnOrder("Tofu", "large");
        }

        protected void OnOrder(string dishName, string size)
        {
            if (this.Order != null) //判斷事件處理器是否有訂閱
            {
                OrderEventArgs e = new OrderEventArgs();
                e.DishName = dishName;
                e.Size = size;
                this.Order.Invoke(this, e); //在這邊觸發事件並執行Waiter.Action方法，因為主結構有訂閱customer.Order
            }
        }

        public void Action()
        {
            Console.ReadLine();
            this.WalkIn();
            this.SitDown();
            this.Think();
        }
    }
    public class Waiter
    {
        public void Action(Customer customer, OrderEventArgs e)
        {
            Console.WriteLine("I will serve you the dish - {0}", e.DishName);
            double price = 10;
            switch (e.Size)
            {
                case "small":
                    price = price * 0.5;
                    break;
                case "large":
                    price = price * 1.5;
                    break;
                default:
                    break;
            }
            customer.Bill += price;
        }
    }
    //自定義事件
    public class OrderEventArgs : EventArgs //委託事件的參數，記得要繼承EventArgs做為事件使用
    {
        public string DishName { get; set; }
        public string Size { get; set; }
    }
    //OrderEventHandler 這個事件只有 Customer類別能使用
    //OrderEventHandler 用來約束事件處理器的參數型別
    public delegate void OrderEventHandler(Customer customer, OrderEventArgs e);

}
