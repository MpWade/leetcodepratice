﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCodingPratice
{
    internal class LeetCode
    {
        //static void Main(string[] args)
        //{

        //}

        /// <summary>
        /// 20. Valid Parentheses
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool IsValid(string s)
        {
            Stack<char> sta = new Stack<char>();
            try //若一開始就為右括弧則會產生錯誤
            {
                foreach (char c in s)
                {
                    //若為左括弧則放到堆疊
                    if (c == '(' || c == '[' || c == '{')   
                        sta.Push(c);

                    //若為右括弧則判斷是否有對應的左括弧，從stack取出
                    else if (c == ')' && sta.Pop() != '(')  
                        return false;
                    else if (c == ']' && sta.Pop() != '[')
                        return false;
                    else if (c == '}' && sta.Pop() != '{')
                        return false;
                }
            }
            catch
            {
                return false;
            }

            return sta.Count == 0;  //若堆疊中還有括弧表示有括弧沒匹配到ex:"["
        }
    }
}
